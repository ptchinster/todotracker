#!/usr/bin/env python3

import todolistlib as tll
from sys import argv, exit


def main(task, duedate):
    tll.add_prime(task, duedate)


if __name__ == "__main__":
    if len(argv) < 3:
        print(f'Usage: {argv[0]} "string of task" "(date|+number)"')
        exit(-1)

    tll.init()
    main(argv[1], argv[2])

# TODO fix how numbers have to all be reassigned, and only go up to 255
# TODO cant handle null dates
# TODO doesnt handle errors well, especially the 255 max limit one

