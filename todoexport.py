#!/usr/bin/env python3

from sys import exit, argv
from os.path import dirname, realpath
import todolistlib as tll


dbhost = None
dbname = None
username = None
password = None
con = None
cnx = None


def mysql_init():
    global cnx
    global con
    global dbhost
    global dbname
    global username
    global password
    try:
        f = open(dirname(realpath(__file__)) + "/connect.txt", "r")
        lines = f.read().splitlines()
        f.close()
        dbhost = lines[0]
        dbname = lines[1]
        username = lines[2]
        password = lines[3]
    except:
        print(
            "Problem reading connect.txt. Needs a hostname, db name, username and password."
        )
        exit(-1)

    try:
        con = mariadb.connect(user=username, password=password, database=dbname)
        cnx = con.cursor()
    except Exception as e:
        print(f"Problem connecting to the database: {e}")
        exit(-1)

    del password


def closeout():
    global con
    con.close()


def main(tblname):
    global cnx
    qq = f"select * from {tblname}"

    cnx.execute(qq)
    tll.do_export(cnx.fetchall())


if __name__ == "__main__":
    tblname = None
    if len(argv) < 2:
        print(f"Usage: {argv[0]} <mysql|sqlite>")
        exit(0)
    if argv[1] == "mysql":
        import mariadb

        tblname = "tasklist"
        mysql_init()
    elif argv[1] == "sqlite":
        tblname = "todolist"
        tll.init()
        con = tll.con
        cnx = con.cursor()
    else:
        print(f"Usage: {argv[0]} <mysql|sqlite>")
        exit(0)

    main(tblname)
    closeout()
