#!/usr/bin/env python3

from sys import exit, argv
from os.path import dirname, realpath
import todolistlib as tll

cnx = None


if __name__ == "__main__":
    if len(argv) < 2:
        print(f"usage: {argv[0]} <csv file to import>")
        exit(0)
    tll.init()
    tll.do_import(argv[1])
