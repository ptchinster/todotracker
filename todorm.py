#!/usr/bin/env python3

import todolistlib as tll
from sys import argv


def main(args):
    cursor = tll.con.cursor()

    for v in args:
        statement = "delete from todolist where idn=?"
        cursor.execute(statement, (v,))
    tll.con.commit()


if __name__ == "__main__":
    tll.init()
    main(argv[1:])
