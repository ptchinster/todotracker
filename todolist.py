#!/usr/bin/env python3

import todolistlib as tll
from sys import argv as sysargv


def main(args):
    searchterm = ""
    if len(args):
        searchterm = " ".join(args)

    crsr = tll.con.cursor()

    qq = f"select * from todolist WHERE duedate IS NOT NULL AND desc LIKE '%{searchterm}%' ORDER BY duedate asc"
    crsr.execute(qq)
    tll.print_results(crsr.fetchall())

    qq = f"select * from todolist WHERE duedate IS NULL AND desc LIKE '%{searchterm}%' ORDER BY idn asc"
    crsr.execute(qq)
    tll.print_results(crsr.fetchall())


if __name__ == "__main__":
    tll.init()
    main(sysargv[1:])
