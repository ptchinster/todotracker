#!/usr/bin/env python3

import todolistlib as tll


def main():
    qq = "SELECT * from todolist WHERE duedate < date('now', 'localtime')"
    idns = []

    cursor = tll.con.cursor()
    data = cursor.execute(qq)

    for idn, desc, duedate in data:
        idns.append(idn)

    for idn in idns:
        qq = f"UPDATE todolist SET duedate=date('now') WHERE idn=?"
        cursor.execute(qq, (idn,))
    tll.con.commit()


if __name__ == "__main__":
    tll.init()
    main()
