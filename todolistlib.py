#!/usr/bin/env python3

import re
import argparse
import csv
import traceback
import sqlite3 as sl
import subprocess  # nosec b404
from os.path import abspath, dirname, exists
from sys import exit as sysexit
from time import sleep
from datetime import date
from requests import head

curdir = f"{dirname(abspath(__file__))}"
dbfile = f"{curdir}/todolist.db"

con = None
crsr = None

MAIL_EXE = "/usr/bin/mail"

# TODO add a todochange


class bcolors:
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    RED = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"  # TODO dont use bold, perl script did on all


"""

#print color 'reset';
#print color 'bold red';
#print "ID\t";
#print color 'reset';
#print color 'bold green';
#print "\t\tTask";
#print color 'reset';
#print "\n\n";
"""


def init():
    global con
    global crsr
    if not exists(dbfile):
        print("No todolist database detected. Do you want to create a new one? [y/N]: ")
        ans = input()
        if ans.lower() != "y":
            sysexit(0)
        con = sl.connect(dbfile)
        with con:
            con.execute(
                """
                CREATE TABLE todolist (
                idn INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                desc TEXT NOT NULL,
                duedate TEXT
                );
            """
            )
        con.commit()
    else:
        con = sl.connect(dbfile)

    crsr = con.cursor()


def list_prime():
    global crsr
    data = crsr.execute("SELECT * FROM todolist")
    return data


def do_list(args):
    data = list_prime()
    bColor = True
    if args.fin("--no-color"):
        bColor = False
    print_results(data, color=bColor)


def do_export(rows):
    today = str(date.today()).replace("-", "")
    outfile = f"{curdir}/todolist.BAK.{today}"
    with open(outfile, mode="w") as file:
        csvFile = csv.writer(file)
        for duedate, desc, idn in rows:
            duedate = str(duedate)
            d = list((desc, duedate.split(" ")[0]))
            csvFile.writerow(d)
    print(f"Wrote to {outfile}")


def do_import(f):
    importFile = f.strip("'")
    with open(f"{curdir}/{importFile}", mode="r") as file:
        csvFile = csv.reader(file)
        for desc, duedate in csvFile:
            add_prime(f"{desc}", f"{duedate}")


def print_results(result, color=True):
    for idn, desc, duedate in result:
        duedate = str(duedate).split(" ")[0]
        if color:
            print(
                f"{bcolors.RED}{duedate}{bcolors.ENDC}\t{bcolors.BLUE}{idn}{bcolors.ENDC}\t{bcolors.GREEN}{desc}{bcolors.ENDC}"
            )
        else:
            print(f"{duedate}\t{idn}\t{desc}")


def do_add(args):
    add_prime(args.name, args.date)


def add_prime(desc, duedate):
    global con
    global crsr

    # check to see if date is an offset or an actual date

    if duedate.startswith("+"):
        if len(duedate) < 2:
            print("You must pass in +SOMENUMBER")
            return

        m = re.search(r"\+([\d]+)([wmy])", duedate)
        if m != None:
            # Handle [w]eek [m]onth and [y]ear multipliers
            qq = None
            match m.group(2):
                case "w":
                    num = int(m.group(1)) * 7
                    qq = f"SELECT DATE(CURRENT_DATE, '+{num} DAY')"
                case "m":
                    num = int(m.group(1))
                    qq = f"SELECT DATE(CURRENT_DATE, '+{num} MONTH')"
                case "y":
                    num = int(m.group(1))
                    qq = f"SELECT DATE(CURRENT_DATE, '+{num} YEAR')"
            crsr.execute(qq)
            duedate = str(crsr.fetchone()[0])
        else:
            # offset, need a real date
            qq = f"SELECT DATE('now', '+{duedate[1:]} days', 'localtime')"
            crsr.execute(qq)
            duedate = str(crsr.fetchone()[0])

    # duedate is now a string like: 2022-05-01
    regex = re.search(r"^\d{4}-\d{2}-\d{2}$", duedate)  # TODO accept YYYYMMDD as well
    if regex is None:
        print(
            f'The string "{duedate}" is not in the format "YYYY-MM-DD" (Name: {desc})'
        )
        return
    # TODO check to make sure dates are valid?

    sql = f"INSERT INTO todolist (desc, duedate) values('{desc}', date('{duedate}'))"
    try:
        crsr.execute(sql)
        con.commit()
    except Exception as e:
        print(f"Error!: {e}")
        traceback.print_exc()
        sysexit(-1)

    print(f'Added "{desc}" with a duedate of "{duedate}"\n')


def have_internet(timeout):
    try:
        head("http://www.google.com/", timeout=timeout)
        return True
    except requests.ConnectionError:
        return False
