#!/usr/bin/env python3

import subprocess
import todolistlib as tll
from time import sleep

MAIL_EXE = "/usr/bin/mail"


def backoff_algorithm(m):
    u = 1
    v = 1
    if m < 0:
        return 15
    elif m == 0:
        return u
    elif m == 1:
        return v
    else:
        for i in range(2, m):
            c = u + v
            u = v
            v = c
        return v


def main():
    cursor = tll.con.cursor()
    qq = f"select * from todolist where duedate=date('now')"
    data = cursor.execute(qq)
    idns = []
    i = 0
    for idn, task, duedate in data:
        #print(f"{idn}, {task}, {duedate}")
        sp = subprocess.Popen( [ MAIL_EXE, "-A", "gmail", "-s", "TODO", "ptchinster@gmail.com", ], stdin=subprocess.PIPE, shell=False,)
        sp.communicate(input=task.encode())
        sleep(backoff_algorithm(i))
        i += 1
        idns.append(idn)

    for idn in idns:
        statement = "delete from todolist where idn=?"
        cursor.execute(statement, (idn,))
    tll.con.commit()


if __name__ == "__main__":
    tll.init()
    main()

